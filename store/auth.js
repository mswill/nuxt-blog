'use strict';
import Cookie from 'cookie'
import Cookies from 'js-cookie'
import jwtDecode from 'jwt-decode'

export const state = () => ( {
    token: null
} )


export const mutations = {
    setToken( state, token ) {
        state.token = token
    },
    clearToken( state ) {
        state.token = null
    }
}
export const actions = {
    async LOGIN( { commit, dispatch }, formData ) {
        try {
            const { token } = await this.$axios.$post( '/api/auth/admin/login', formData )
            dispatch( 'SET_TOKEN', token )
        } catch ( e ) {
            commit( 'setError', e, { root: true } )
            throw e
        }
    },
    SET_TOKEN( { commit }, token ) {
        this.$axios.setToken( token, 'Bearer' )
        commit( 'setToken', token )
        Cookies.set( 'jwt-token', token )
    },
    async CREATE_USER( { commit }, formData ) {
        try {
            await this.$axios.post( '/api/auth/admin/create', formData )
        } catch ( e ) {
            commit( 'setError', e, { root: true } )
            throw e
        }
    },
    AUTO_LOGIN( { dispatch } ) {
        const cookieStr = process.browser
            ? document.cookie
            : this.app.context.req.headers.cookie

        // достаем куки и если вдруг на сервере ничего нет, возвращаем пустой объект {}
        const cookies = Cookie.parse( cookieStr || '' ) || {}
        const token = cookies[ 'jwt-token' ]

        if ( isJwtValid( token ) ) {
            dispatch( 'SET_TOKEN', token )
        } else {
            dispatch( 'LOGOUT' )
        }

    },
    LOGOUT( { commit }, ) {
        this.$axios.setToken( false )
        commit( 'clearToken' )
        Cookies.remove( 'jwt-token' )
    }
}
export const getters = {
    isAuthenticated: state => Boolean( state.token ),
    token: state => state.token
}


function isJwtValid( token ) {
    if ( !token ) {
        return false
    }

    const jwtData = jwtDecode( token ) || {}
    const expires = jwtData.exp || 0

    return ( new Date().getTime() / 1000 ) < expires
}
