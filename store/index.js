'use strict';

export const state = () => ({
    error: null
})


export const actions = {
    nuxtServerInit( { dispatch } ) {
       dispatch('auth/AUTO_LOGIN')
    }
}
export const mutations = {
    setError(state, error) {
        state.error = error
    },
    clearError(state){
        state.error = null
    }
}
export const getters = {
    error: state => state.error
}
