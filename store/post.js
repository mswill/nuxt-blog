'use strict';
import ca from "element-ui/src/locale/lang/ca"

const posts = []
export const actions = {
    async CREATE( { commit }, { title, text, image } ) {
        try {
            const fd = new FormData()
            fd.append( 'title', title )
            fd.append( 'text', text )
            fd.append( 'image', image, image.name )
            return await this.$axios.post( '/api/post/admin', fd )

        } catch ( e ) {
            commit( 'setError', e, { root: true } )
            throw e
        }
    },
    async FETCH_ADMIN( { commit } ) {
        try {
            return await this.$axios.$get( '/api/post/admin' )
        } catch ( e ) {
            commit( 'setError', e, { root: true } )
            throw e
        }
    },
    async FETCH_ADMIN_BY_ID( { commit }, id ) {
        try {
            return await this.$axios.$get( `/api/post/admin/${ id }` )
        } catch ( e ) {
            commit( 'setError', e, { root: true } )
            throw e
        }
    },

    async FETCH( { commit } ) {
        try {
            return await this.$axios.$get( '/api/post' )
        } catch ( e ) {
            commit( 'setError', e, { root: true } )
            throw e
        }
    },
    async FETCH_BY_ID( { commit }, id ) {
        try {
            return await this.$axios.$get( `/api/post/${ id }` )
        } catch ( e ) {
            commit( 'setError', e, { root: true } )
            throw e
        }
    },
    async ADD_VIEW( { commit }, { views, _id } ) {
        try {
            return await this.$axios.$put( `/api/post/add/view/${ _id }`, { views } )
        } catch ( e ) {
            commit( 'setError', e, { root: true } )
            throw e
        }
    },
    async REMOVE( { commit }, id ) {
        try {
            return await this.$axios.$delete( `/api/post/admin/${ id }` )
        } catch ( e ) {
            commit( 'setError', e, { root: true } )
            throw e
        }
    },
    async UPDATE( { commit }, { id, text } ) {
        try {
            return await this.$axios.$put( `/api/post/admin/${ id }`, { text } )
        } catch ( e ) {
            commit( 'setError', e, { root: true } )
            throw e
        }
    },

    async GET_ANALYTIC( { commit } ) {
        try {
            return this.$axios.$get( '/api/post/admin/get/analytics' )
        } catch ( e ) {
            commit( 'setError', e, { root: true } )
        }

    }
}
