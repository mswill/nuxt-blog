export default {
    mode: 'universal',
    target: 'server',

    head: {
        title: process.env.npm_package_name || '',
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'description', name: 'description', content: process.env.npm_package_description || '' },
        ],
        link: [
            { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
            {
                href: 'https://fonts.googleapis.com/css2?family=Montserrat:wght@100;200;300;400;500;600&display=swap',
                rel: 'stylesheet'
            }
        ]
    },
    loading: { color: '#8F9EFF' },
    css: [
        'element-ui/lib/theme-chalk/index.css',
        '@/assets/sass/main.scss'
    ],

    plugins: [
        '@/plugins/globals',
        '@plugins/axios'
    ],

    components: true,

    buildModules: [],

    modules: [
        // Doc: https://axios.nuxtjs.org/usage
        '@nuxtjs/axios',
    ],

    axios: {},

    build: {
        transpile: [ /^element-ui/ ],
    },
    /*
     ** Add server middleware
     ** Nuxt.js uses `connect` module as server
     ** So most of express middleware works with nuxt.js server middleware
     */

    serverMiddleware: [
        '~/api'
    ]
}
