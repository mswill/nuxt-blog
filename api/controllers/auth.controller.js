'use strict';
const bcrypt = require( 'bcrypt-nodejs' )
const jwt = require ('jsonwebtoken')
const User = require( '../models/user.model' )
const keys = require( '../keys' )

module.exports.login = async ( req, res ) => {

    // Ищем пользователя
    const candidate = await User.findOne( {
        login: req.body.login
    } )

    // Если такой есть
    if ( candidate ) {
        // Проверяем его пароль на соответсвие
        const isPassCorrect = bcrypt.compareSync( req.body.password, candidate.password )

        // если пароли совпали
        if ( isPassCorrect ) {
            // создаем токен
            const token = jwt.sign( {
                login: candidate.login,
                userId: candidate._id
            }, keys.JWT, { expiresIn: 60 * 60 } )

            // Отправляем токен
            res.json( { token } )
        } else {
            // если пароли не совпали, отправлям ошибку
            res.status( 401 ).json( { message: 'Пароль не верный' } )
            // res.status( 404 ).json( { message: 'Пользователь не найден' } )
        }
    } else {
        // если ползователь не найден, отправляем ошибку
        res.status( 404 ).json( { message: 'Пользователь не найден' } )
    }
}
module.exports.createUser = async ( req, res ) => {

    const candidate = await User.findOne({ login: req.body.login})

    if ( candidate ) {
        res.status(409).json({ message: 'Такой логин уже занят'})
    }else{
        const salt = bcrypt.genSaltSync(10)
        const user = new User({
            login: req.body.login,
            password: bcrypt.hashSync(req.body.password, salt)
        })
        await user.save()

        res.status(201).json(user)
    }
}
