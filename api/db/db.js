'use strict';
const blueBird = require( 'bluebird' )
const keys = require('../keys')

const mongoose = require('mongoose');
mongoose.Promise = blueBird.Promise

mongoose.connect( keys.dbPath, keys.dbOption );

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error: ---- '));
db.once('open', ()=> {
    console.log("MongoDB Connected...");
});

module.exports = db

