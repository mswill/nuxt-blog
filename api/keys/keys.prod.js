'use strict';

module.exports = {
    dbOption: { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false, useCreateIndex: true, },
    dbPath: process.env.MONGO_URI,
    JWT: process.env.JWT
};
