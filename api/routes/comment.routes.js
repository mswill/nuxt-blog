'use strict';
const { Router } = require( 'express' )
const router = Router()
const passport = require( 'passport' )
const ctr  = require( '../controllers/comment.controller' )

// - /api/comment
router.post('/',  ctr.create)

module.exports = router

