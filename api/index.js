'use strict';
const express = require( 'express' )
const passport = require('passport')
const morgan = require('morgan')
// const cors = require('cors')
// const csurf = require( 'csurf' )
const helmet = require( 'helmet' )
// const MongoStore = require( 'connect-mongodb-session' )( session );
// const flash = require( 'connect-flash' )
// const path = require( 'path' );
// const morgan = require( 'module' );
// const session = require( 'express-session' );





const db = require( './db/db' )
const app = express()
app.use(helmet())
app.use(passport.initialize())
const passportStrategy = require('./server.middleware/passport-straegy')
passport.use(passportStrategy)
const keys = require( './keys' )

app.use( express.urlencoded( { extended: true } ) )
app.use( express.json() );

// import routes
const authRoute = require( './routes/auth.routes' )
const postRoutes = require( './routes/post.routes' )
const commentRoutes = require( './routes/comment.routes' )
// Routes

app.use(morgan('dev'))
app.use('/auth', authRoute )
app.use('/post', postRoutes )
app.use('/comment', commentRoutes )


module.exports = {
    path: '/api',
    handler: app
}



